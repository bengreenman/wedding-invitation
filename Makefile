NAME=invitation
RSVP=details

all: details.pdf invitation.pdf

details.pdf: details.tex
	latexmk --pdf $<

invitation.pdf: invitation.tex
	latexmk --pdf $<

clean:
	latexmk -C
