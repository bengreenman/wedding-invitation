invitation
===

Wedding invitation.

![invitation](img/example/combined.png)


Overview
---

- `invitation.tex` : source for the front page of the invitation
- `details.tex` : source for the back page
- `img/` : images, mostly
- `img/example/` : built copy of the invitation


Build
---

To build the front & back:

```
make
```

There is a TeX `\newif` at the top of each `.tex` file that determines whether
the output is for paper size A5 or size A6.

To build the front only:

```
make invitation.pdf
```

To build the back only:

```
make details.pdf
```


Credits
---

Created using [TeX Live 2017](http://www.tug.org/mactex/)
 and the fonts: Cinzel, Linux Libertine, Inconsolata, and Rothenberg Decorative.
The border is from the title page of
 [_Bible Pictures and Stories_ (1892)](https://www.fromoldbooks.org/DJD-BiblePictures-NewTestament/pages/000-title-Page-border/)
 via [fromoldbooks.org](https://www.fromoldbooks.org/).

Thanks to this `tex.stackexchange.com` thread for inspiration:

<https://tex.stackexchange.com/questions/281415/showcase-of-beautiful-invitations-in-tex>

